import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {HmsPipe} from './hms.pipe';
import {StopwatchComponent} from './components/stopwatch/stopwatch.component';
import {TimerComponent} from './components/timer/timer.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCardModule, MatIconModule, MatInputModule} from '@angular/material';
import {FormsModule} from '@angular/forms';
import {PwaService} from './pwa.service';

@NgModule({
  declarations: [
    AppComponent,
    HmsPipe,
    StopwatchComponent,
    TimerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatInputModule,
    MatCardModule,
    FormsModule,
    MatButtonModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [PwaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
