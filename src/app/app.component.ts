import {Component, ComponentFactoryResolver, ViewChild, ViewContainerRef} from '@angular/core';
import {PwaService} from './pwa.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-pwa-desktop';
  constructor(public Pwa: PwaService)  {

  }

  installPwa(): void {
    this.Pwa.promptEvent.prompt();
  }



}
