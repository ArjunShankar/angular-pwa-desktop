import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PwaService {

  public promptEvent: any;

  constructor() {
    window.addEventListener('beforeinstallprompt', event => {
      console.log('beforeinstallprompt was fired....')
      this.promptEvent = event;
    });
  }
}
