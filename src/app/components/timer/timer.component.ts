import { Component, OnInit } from '@angular/core';

import {Subscription, timer} from 'rxjs';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html'
})
export class TimerComponent implements OnInit {

  private startPoint: number = 30;
  public ticks: number;
  timer: any;
  private sub: Subscription;

  constructor() {
    this.ticks = this.startPoint;
  }

  ngOnInit() {
    this.timer = timer(1000, 1000);
    this.sub = this.timer.subscribe(t => {

      this.ticks = this.startPoint - t;
      if (this.ticks === 0) {
        this.sub.unsubscribe();
      }
    });



  }

}
