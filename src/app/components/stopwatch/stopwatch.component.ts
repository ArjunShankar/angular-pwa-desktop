import { Component, OnInit } from '@angular/core';
import {timer} from 'rxjs';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.component.html'
})


  export class StopwatchComponent {

  constructor() {
    this.status = 'stopped';
  }

  public status: string;
  ticks = 0;
  timer: any;
  private sub: Subscription;
  state = 0;

  start() {
    this.timer = timer(1000, 1000);
    this.sub = this.timer.subscribe(t => this.ticks = t);
    this.status = 'running';
  }

  stop() {
    this.ticks = 0;
    this.state = 0;
    this.status = 'stopped';
    this.sub.unsubscribe();
  }

  pause() {
    this.sub.unsubscribe();
    this.state = this.ticks;
  }

  unpause() {
    this.sub = this.timer.subscribe( t => {
      if( t > 0){
        this.ticks = t + this.state;
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
